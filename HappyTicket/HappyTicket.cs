using System;
using System.Linq;

namespace HappyTicket
{
    public class HappyNumbers
    {  
        private static int maxNumberLength = Int32.MaxValue.ToString().Length;

        private bool noHappyNumbers = true;
        private bool firstTrig = false;

        private int numberLength = 0;
        private int lastHappyNumber = 0;

        private int startRange = 0;
        private int endRange = 0;

        /// <summary>  
        /// Конструктор класса  
        /// </summary>  
        /// <param name="length">Длина счастливого билетикаа</param>  
        public HappyNumbers(int length)
        {
            if ((length > 1) && (length <= maxNumberLength) && IsEven(length)) {
                noHappyNumbers = false;
                numberLength = length;
                startRange = (int) Math.Pow(10, numberLength - 1);
                endRange = (numberLength == maxNumberLength) ? Int32.MaxValue : (int) Math.Pow(10, numberLength) - 1;
            }
        }
 
        /// <summary>  
        /// Возвращает следующий по порядку счастливый билетик или 0, если таких номеров больше нет
        /// </summary>  
        /// <returns>Например, 12344321 (для длины номера 8) или 0 (для длины номера 1)</returns>  
        public int GetHappyNumber()  
        {  
            if (noHappyNumbers) {
                return 0;
            } else if (!firstTrig) {
                lastHappyNumber = GenHappy(startRange / ((int) Math.Pow(10, numberLength / 2)), startRange);
                firstTrig = true;
            } else {
                lastHappyNumber = GenHappy(lastHappyNumber / ((int) Math.Pow(10, numberLength / 2)), lastHappyNumber);
            }
            return lastHappyNumber;
        }

        public int GenHappy(int firstHalf, int lastHappy) {
            int sumFirstHalf = SumDigits(firstHalf);
            int halfLastHappy = lastHappy % ((int) Math.Pow(10, numberLength / 2));
            int maxHalf = (int) Math.Pow(10, numberLength / 2) - 1;
            while (true) {
                if (halfLastHappy < maxHalf) {
                    halfLastHappy++;
                } else if (firstHalf < maxHalf) {
                    halfLastHappy = 0;
                    firstHalf++;
                    sumFirstHalf = SumDigits(firstHalf);
                } else {
                    noHappyNumbers = true;
                    return 0;
                }
                if (sumFirstHalf == SumDigits(halfLastHappy)) {
                    string formatString = String.Concat("D", numberLength / 2);
                    return Convert.ToInt32(string.Format("{0}{1}", firstHalf, halfLastHappy.ToString(formatString)));
                }
            }
        }

        /// <summary>
        /// Возвращает сумму цифр в числе
        /// </summary>
        /// <param name="num">Число</param> 
        private static int SumDigits(int num) {
            int sum = 0;
            while (num != 0) {
                sum += num % 10;
                num /= 10;
            }
            return sum;
        }

        /// <summary>
        /// Возвращает последний счастливый билет
        /// </summary>
        public int LastHappyNumber {
            get { return lastHappyNumber; }
        }
        
        /// <summary>
        /// Возвращает true, если число нечетное
        /// </summary>
        /// <param name="number">Число</param>  
        private static bool IsEven(int number) {
            return (number % 2 == 0);
        }
    }
}
