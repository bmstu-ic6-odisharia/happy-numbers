# Счастливый билетик

Счастливым называется билет, номер которого отвечает следующим условиям:
 - количество цифр в номере билета четное
 - сумма цифр первой половины номера равна сумме цифр второй половины номера

Пример: 12344321 - счастливый номер, 12340307 - счастливый номер, 12345678 - несчастливый номер.

Реализовать класс для генерации всех счастливых номеров. Особенность реализации состоит в том, что номера должны формироваться последовательно. Каждый новый вызов метода GetHappyNumber должен формировать следующий счастливый номер. Если счастливых номеров больше нет, следует вернуть 0. Длина номера передается в виде параметра конструктору класса. 

Класс должен иметь следующий вид:

```csharp  
public class HappyNumbers
{  
    /// <summary>  
    /// Конструктор класса  
    /// </summary>  
    /// <param name="length">Длина счастливого билета в цифрах (четное число)</param>  
    public HappyNumbers(int length)  
    {  
    }  
    /// <summary>  
    /// Вернуть следующий по порядку счастливый номер или 0, если таких номеров больше нет  
    /// </summary>  
    /// <returns></returns>  
    public int GetHappyNumber()  
    {  
    }  
}
```
## Восстановление проекта

```
dotnet restore HappyTicket
dotnet restore HappyTicket.Tests
```


## Запуск тестов

```
dotnet test HappyNumbers.Tests/HappyNumbers.Tests.csproj 
```

